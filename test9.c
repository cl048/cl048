#include <stdio.h>
void in(int *a,int *b)
{
printf("Enter 2 numbers say a and b\n");
scanf("%d%d",a,b);
}
void swap_call_by_value(int a,int b)
{
int c;
c=a;
a=b;
b=c;
printf("In swap_call_by_value new values of a and b are %d and %d respectively\n",a,b);
}
void swap_call_by_reference(int *a,int *b)
{
*a=*a+*b;
*b=*a-*b;
*a=*a-*b;;
printf("In swap_call_by_reference new values of a and b are %d and %d respectively\n",*a,*b);
}
int main()
{
int a,b;
in(&a,&b);
printf("In main values of a and b are %d and %d respectively\n",a,b);
swap_call_by_value(a,b);
swap_call_by_reference(&a,&b);
return 0;
}