#include <stdio.h>

int main()
{
    int arr[100], i, j, n, Count = 0;

    printf("\n enter the number of elements \t");
    scanf("%d",&n);

    printf("\n Enter %d elements of an Array  : \n ", n);
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }     

    for (i = 0; i < n; i++)
    {
        for(j = i + 1; j < n; j++)
        {
            if(arr[i] == arr[j])
            {
                Count++;
            }
        }
    }

    printf("\n Total Number of Duplicate Elements in this Array  =  %d ", Count);

    return 0;
}