# include<stdio.h>
int main()
{
	int mat[11][11];
	int i,j,m,n;
	printf("enter the number of elements\t");
	scanf("%d%d",&m,&n);
	printf("enter the elements\n");
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			scanf("%d",&mat[i][j]);
		}
	}
	printf("The entered matrix is :\n");
	for(i=0;i<m;i++)
	{
		printf("\n");
		for(j=0;j<n;j++)
		{
			printf("\t%d",mat[i][j]);
		}
	}
	int transpose_mat[11][11];
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			transpose_mat[i][j]=mat[j][i];
		}
	}
	printf("\nThe transposed matrix is :\n");
	for(i=0;i<m;i++)
	{
		printf("\n");
		for(j=0;j<n;j++)
		{
			printf("\t%d",transpose_mat[i][j]);
		}
	}
	return 0;
}
