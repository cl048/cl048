#include <stdio.h>
struct student {
char firstName[50];
int roll;
float marks;
} s[10];
int main() 
{
int i;
printf("Enter information of students:\n");
for (i = 0; i < 10; ++i) 
{
printf("Enter roll no. : ");
scanf("%d",s[i].roll);
printf("Enter first name: ");
scanf("%s", s[i].firstName);
printf("Enter marks: ");
scanf("%f", &s[i].marks);
}
printf("Displaying Information:\n");
for (i = 0; i < 10; ++i) 
{
printf("Roll number: %d\n",s[i].roll);
printf("First name: ");
puts(s[i].firstName);
printf("Marks: %.1f", s[i].marks);
printf("\n");
}
return 0;
}