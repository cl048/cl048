#include<stdio.h> 
#include<math.h> 
void in(int *a,int *b,int *c)
{
printf("Enter coeff. of x^2,x^1,x^0\n");
scanf("%d%d%d",a,b,c);
}
void roots(int a, int b, int c) 
{ 
if (a == 0) 
{ 
printf("Invalid"); 
return; 
}
int d = b*b - 4*a*c;
double sqrt_val = sqrt((d>0?d:-d));
if (d > 0) 
{ 
printf("Roots are real and different \n"); 
printf("%f\n%f",(double)(-b + sqrt_val)/(2*a) , (double)(-b - sqrt_val)/(2*a)); 
} 
else if (d == 0) 
{ 
printf("Roots are real and same \n"); 
printf("%f",-(double)b / (2*a)); 
} 
else 
{ 
printf("Roots are complex \n"); 
printf("%f + i%f\n%f - i%f", -(double)b / (2*a),sqrt_val,-(double)b / (2*a), sqrt_val); 
} 
}
int main() 
{ 
int a,b,c;
in(&a,&b,&c);
roots(a, b, c); 
return 0; 
} 